# $OpenBSD: dot.profile,v 1.4 2005/02/16 06:56:57 matthieu Exp $
#
# sh/ksh initialization

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:.

GOPATH=~/go
PATH=$GOPATH/bin:$PATH
PATH=$HOME/.local/bin:$PATH
PATH=$HOME/.cargo/bin:$PATH

export GOPATH PATH HOME TERM

export PKG_PATH="http://ftp.fr.openbsd.org/pub/OpenBSD/snapshots/packages/amd64/"

export LC_CTYPE="en_US.UTF-8"

ENV=${HOME}/.kshrc
