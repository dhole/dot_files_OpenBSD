[ "$(which colorls 2>/dev/null)"  ] && alias ls='colorls -G'
alias free='top -n | grep ^Memory'
alias ll='ls -l'
alias la='ls -la'
alias sudo="sudo -E"
alias tmux="tmux -2"

if [ "$TERM" = "xterm" ]
then
    export TERM="xterm-256color"
fi

RED="196"
ORANGE="208"
YELLOW="226"
GREEN="76"
GRAY="246"
BLUE="39"
PURPLE="105"
BLACK="15"
WHITE="15"
PINK="200"
RESET="\e[0m"

case "$USER" in
"dev")
    USER_COLOR=$GREEN
    ;;
"media")
    USER_COLOR=$ORANGE
    ;;
"browsing")
    USER_COLOR=$YELLOW
    ;;
*)
    USER_COLOR=$PINK
    ;;
esac

git_branch_name() {
    val=`git branch 2>/dev/null | grep '^*' | colrm 1 2`
    echo "$val"
}

if [ "$TERM" = "xterm-256color" ]; then
    PS1='\[\e[38;5;${USER_COLOR}m\e[1m\]\u@\h\[${RESET}\] \[\e[38;5;${BLUE}m\]\w\[${RESET}\] \[\e[38;5;${GRAY}m\]$(git_branch_name)\n \[\e[38;5;${WHITE}m\]\$\[${RESET}\] '
else
    PS1='\u@\h:\w\ '
fi

#if [ $LOGNAME = 'root' ]; then
#	PS1='\e[1;31m\]\u \w $\[\e[0m\] '
#else
#	PS1='\e[1;32m\]\u \w $\[\e[0m\] '
#fi
#PS1='\[\e[1;32m\][\u@\h \w]\$\[\e[0m\] '
export PS1

if [ -f ~/.ksh_aliases ]; then
    . ~/.ksh_aliases
fi

export VISUAL=vim
export EDITOR=vim
export PAGER=less

export RUST_SRC_PATH=$HOME/BigGit/rust/src

set -o emacs
